﻿using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using System.Windows;
using ContactBookWpf.Model;

namespace ContactBookWpf.ViewModel
{
    public class PersonViewModel : ViewModelBase
    {
        public readonly Person Person;

        public PersonViewModel(Person person)
        {
            Person = person;
        }

        public int Id => Person.Id;
        public string Fullname => GetProperty();
        public ObservableCollection<string> PhoneNumbers => new ObservableCollection<string>(Person.PhoneNumbers);

        public ObservableCollection<CalendarEntry> CalendarEntries =>
            new ObservableCollection<CalendarEntry>(Person.CalendarEntries);

        public string Firstname
        {
            get => GetProperty();
            set
            {
                SetProperty(value);
                OnPropertyChanged(nameof(Person.Fullname));
            }
        }

        public string Surname
        {
            get => GetProperty();
            set
            {
                SetProperty(value);
                OnPropertyChanged(nameof(Person.Fullname));
            }
        }

        public string Lastname
        {
            get => GetProperty();
            set
            {
                SetProperty(value);
                OnPropertyChanged(nameof(Person.Fullname));
            }
        }

        public string Street
        {
            get => GetProperty();
            set => SetProperty(value);
        }

        public string HouseNumber
        {
            get => GetProperty();
            set => SetProperty(value);
        }

        public string Postalcode
        {
            get => GetProperty();
            set => SetProperty(value);
        }

        public string City
        {
            get => GetProperty();
            set => SetProperty(value);
        }

        /// <summary>
        /// Sets the value to the property of the person
        /// </summary>
        /// <param name="value">The value as string that will be set</param>
        /// <param name="propertyName">The property name, when left empty the caller member name will be used</param>
        private void SetProperty(string value, [CallerMemberName] string propertyName = null)
        {
            var propertyInfo = Person.GetType().GetProperty(propertyName);
            if (Equals(propertyInfo?.GetValue(Person, null), value)) return;
            propertyInfo?.SetValue(Person, value);
            OnPropertyChanged();
        }

        /// <summary>
        /// Get the property of the person as a string
        /// </summary>
        /// <param name="propertyName">The property name, when left empty the caller member name will be used</param>
        /// <returns>The property as a string</returns>
        private string GetProperty([CallerMemberName] string propertyName = null)
        {
            var propertyInfo = Person.GetType().GetProperty(propertyName);
            return propertyInfo?.GetValue(Person, null).ToString();
        }

        /// <summary>
        /// Shows a message box to confirm the deletion of the phone number of the person.
        /// </summary>
        /// <param name="index">The index of the phone number in the phone numbers list</param>
        public void DeletePhoneNumber(int index)
        {
            if (index < 0) return;
            var result =
                MessageBox.Show(
                    $"Weet u het zeker dat u telefoon nummer '{Person.PhoneNumbers[index]}' van '{Fullname}' wilt verwijderen?",
                    "Telefoonnummer verwijderen", MessageBoxButton.YesNoCancel);
            if (result != MessageBoxResult.Yes) return;

            Person.PhoneNumbers.RemoveAt(index);
            OnPropertyChanged(nameof(PhoneNumbers));
        }

        /// <summary>
        /// Shows a message box to confirm the deletion of the calendar entry of the person.
        /// </summary>
        /// <param name="index">The index of the calendar entry in the calendar entries list</param>
        public void DeleteCalendarEntry(int index)
        {
            if (index < 0) return;
            var result =
                MessageBox.Show(
                    $"Weet u het zeker dat u kalender item '{Person.CalendarEntries[index].Description}' van '{Fullname}' wilt verwijderen?",
                    "Kalender item verwijderen", MessageBoxButton.YesNoCancel);
            if (result != MessageBoxResult.Yes) return;

            Person.CalendarEntries.RemoveAt(index);
            OnPropertyChanged(nameof(CalendarEntries));
        }
    }
}