﻿using System;
using ContactBookWpf.Model;

namespace ContactBookWpf.ViewModel
{
    public class EditCalendarEntryViewModel : ViewModelBase
    {
        private CalendarEntry _calendarEntry;
        private DateTime _date;
        private string _description;
        private bool _everyYear;
        private string _introText;
        private PersonViewModel _selectedPerson;

        public EditCalendarEntryViewModel()
        {
            Date = DateTime.Now;
        }

        public PersonViewModel SelectedPerson
        {
            get => _selectedPerson;
            set
            {
                SetProperty(ref _selectedPerson, value);
                IntroText =
                    "Voer hieronder de datum en de beschrijving die bij de datum hoort in. " +
                    "Vink ieder jaar aan als het zich ieder jaar herhaald, zoals een verjaardag. " +
                    $"Dit item wordt bijgewerkt bij de onderstaande contactpersoon{Environment.NewLine}" +
                    $"{Environment.NewLine}" +
                    $"Naam: {SelectedPerson.Fullname}{Environment.NewLine}" +
                    $"Adres: {SelectedPerson.Street} {SelectedPerson.HouseNumber}{Environment.NewLine}" +
                    $"Postcode: {SelectedPerson.Postalcode}{Environment.NewLine}" +
                    $"Woonplaats: {SelectedPerson.City}{Environment.NewLine}";
            }
        }

        public string IntroText
        {
            get => _introText;
            set => SetProperty(ref _introText, value);
        }

        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }

        public DateTime Date
        {
            get => _date;
            set => SetProperty(ref _date, value);
        }

        public bool EveryYear
        {
            get => _everyYear;
            set => SetProperty(ref _everyYear, value);
        }

        public CalendarEntry CalendarEntry
        {
            get => _calendarEntry;
            set
            {
                _calendarEntry = value;
                Description = value.Description;
                Date = value.Date;
                EveryYear = value.EveryYear;
            }
        }

        public void SaveCalendarEntry()
        {
            CalendarEntry.Description = Description;
            CalendarEntry.Date = Date;
            CalendarEntry.EveryYear = EveryYear;
        }
    }
}