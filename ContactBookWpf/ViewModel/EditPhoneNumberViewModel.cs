﻿using System;

namespace ContactBookWpf.ViewModel
{
    public class EditPhoneNumberViewModel : ViewModelBase
    {
        private string _introText;
        private string _phoneNumber;
        private int _phoneNumberIndex;
        private PersonViewModel _selectedPerson;

        public PersonViewModel SelectedPerson
        {
            get => _selectedPerson;
            set
            {
                SetProperty(ref _selectedPerson, value);
                IntroText =
                    $"Bewerk hieronder het telefoonnummer van de onderstaande contactpersoon.{Environment.NewLine}" +
                    $"{Environment.NewLine}" +
                    $"Naam: {SelectedPerson.Fullname}{Environment.NewLine}" +
                    $"Adres: {SelectedPerson.Street} {SelectedPerson.HouseNumber}{Environment.NewLine}" +
                    $"Postcode: {SelectedPerson.Postalcode}{Environment.NewLine}" +
                    $"Woonplaats: {SelectedPerson.City}{Environment.NewLine}";
            }
        }

        public string IntroText
        {
            get => _introText;
            set => SetProperty(ref _introText, value);
        }

        public string PhoneNumber
        {
            get => _phoneNumber;
            set => SetProperty(ref _phoneNumber, value);
        }

        public int PhoneNumberIndex
        {
            get => _phoneNumberIndex;
            set
            {
                _phoneNumberIndex = value;
                PhoneNumber = SelectedPerson.Person.PhoneNumbers[PhoneNumberIndex];
            }
        }

        public void SavePhoneNumber()
        {
            SelectedPerson.Person.PhoneNumbers[PhoneNumberIndex] = PhoneNumber;
        }
    }
}