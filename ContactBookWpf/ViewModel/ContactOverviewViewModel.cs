﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using ContactBookWpf.Model;
using ContactBookWpf.Model.Handler;
using ContactBookWpf.View;
using Microsoft.Win32;
using Newtonsoft.Json;
using OfficeOpenXml;

namespace ContactBookWpf.ViewModel
{
    public class ContactOverviewViewModel : ViewModelBase
    {
        private readonly string _excelFiles;
        private int _calendarListSelectedIndex;
        private CalendarSheet _calendarSheet;
        private ObservableCollection<PersonViewModel> _contacts;

        private ContactsSheet _contactsSheet;
        private int _phoneNumberListSelectedIndex;
        private int _selectedIndex;
        private PersonViewModel _selectedPerson;

        private const string ContactsExcel = "Contacten Excel";
        private const string MessageText =
            "Selecteer een Excel bestand met daarin contacten. Klik op 'Oke' om door te gaan of op 'Annuleren' om een leeg werkblad te maken.";

        public ContactOverviewViewModel()
        {
            _excelFiles = "Excel Files|*.xls;*.xlsx;*.xlsm";

            LoadSheets(ContactsExcel, MessageText);
            LoadContacts();
        }

        private Dictionary<string, List<List<string>>> Sheets
        {
            set
            {
                value.TryGetValue(ContactsSheet.Name, out var contactSheetData);
                _contactsSheet = new ContactsSheet(contactSheetData);
                value.TryGetValue(CalendarSheet.Name, out var calendarSheetData);
                _calendarSheet = new CalendarSheet(calendarSheetData);
            }
        }

        public ObservableCollection<PersonViewModel> Contacts
        {
            get => _contacts;
            set => SetProperty(ref _contacts, value);
        }

        public int SelectedIndex
        {
            get => _selectedIndex;
            set
            {
                if (value < 0) value = 0;
                SetProperty(ref _selectedIndex, value);
                SelectedPerson = Contacts[value];

                PhoneNumberListSelectedIndex = 0;
                SelectedPerson.PropertyChanged += OnSelectedPersonOnPropertyChanged;
            }
        }

        public int PhoneNumberListSelectedIndex
        {
            get => _phoneNumberListSelectedIndex;
            set => SetProperty(ref _phoneNumberListSelectedIndex, value);
        }

        public int CalendarListSelectedIndex
        {
            get => _calendarListSelectedIndex;
            set => SetProperty(ref _calendarListSelectedIndex, value);
        }

        public PersonViewModel SelectedPerson
        {
            get => _selectedPerson;
            set => SetProperty(ref _selectedPerson, value);
        }

        public double MaxHeight { get; } = SystemParameters.MaximizedPrimaryScreenHeight;
        public double MaxWidth { get; } = SystemParameters.MaximizedPrimaryScreenWidth;

        private void LoadSheets(string messageCaption, string messageText, bool forceOpenFile = false)
        {
            if (Settings.Default.Workbook != string.Empty && !forceOpenFile)
            {
                var excelFile = new ExcelFile(Settings.Default.Workbook);
                Sheets = excelFile.GetSheetsAsDict(new BackgroundWorker());
                return;
            }

            var response = MessageBox.Show(
                messageText,
                messageCaption,
                MessageBoxButton.OKCancel);

            Settings.Default.Workbook = "Contacten.xlsx";

            if (response == MessageBoxResult.OK)
            {
                var fileName = OpenExcelDialog(_excelFiles);
                if (fileName != string.Empty)
                {
                    var excelFile = new ExcelFile(fileName);
                    Sheets = excelFile.GetSheetsAsDict(new BackgroundWorker());
                    Settings.Default.Workbook = fileName;
                }
            }

            Settings.Default.Save();
        }

        private static string OpenExcelDialog(string fileFilter)
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = fileFilter
            };

            var userClicked = openFileDialog.ShowDialog();

            return userClicked != true ? string.Empty : openFileDialog.FileName;
        }

        private void LoadContacts()
        {
            var contacts = _contactsSheet.GetContacts();
            Contacts = new ObservableCollection<PersonViewModel>(contacts);
            _calendarSheet.AddCalendarEntriesToContacts(contacts);
            SelectedIndex = 0;

            CheckEmptyPerson();
        }

        /// <summary>
        ///     Checks if an empty person should be added to the contacts list or removed.
        /// </summary>
        private void CheckEmptyPerson()
        {
            var emptyPerson = Contacts.ToList().FindAll(x => x.Person.IsEmpty());

            if (emptyPerson.Count == 0)
                Contacts.Add(new PersonViewModel(new Person()));
            else if (emptyPerson.Count > 1 && SelectedPerson.Id != emptyPerson.Last().Id)
                Contacts.Remove(emptyPerson.Last());
        }

        private void OnSelectedPersonOnPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName.Equals(nameof(PersonViewModel.Fullname))) CheckEmptyPerson();
        }

        public void DeletePhoneNumberButtonClick()
        {
            SelectedPerson.DeletePhoneNumber(PhoneNumberListSelectedIndex);
        }

        /// <summary>
        /// Opens add phone number view, when that returns true (the phone number is added), then on property changed of SelectedPerson will be called, so the view will be refreshed
        /// </summary>
        public void AddPhoneNumberButtonClick()
        {
            var phoneNumberAdded = new AddPhoneNumberView(SelectedPerson).ShowDialog();
            if (phoneNumberAdded != null && (bool) !phoneNumberAdded) return;
            OnPropertyChanged(nameof(SelectedPerson));
        }

        /// <summary>
        /// Opens edit phone number view, when that returns true (the phone number is saved), then on property changed of SelectedPerson will be called, so the view will be refreshed
        /// </summary>
        public void EditPhoneNumberButtonClick()
        {
            var phoneNumberSaved = new EditPhoneNumberView(SelectedPerson, PhoneNumberListSelectedIndex).ShowDialog();
            if (phoneNumberSaved != null && (bool) !phoneNumberSaved) return;
            OnPropertyChanged(nameof(SelectedPerson));
        }

        public void DeleteContactButtonClick()
        {
            Contacts.Remove(SelectedPerson);
            CheckEmptyPerson();
        }

        public void AddCalendarEntryButtonClick()
        {
            var calendarEntryAdded = new AddCalendarEntryView(SelectedPerson).ShowDialog();
            if (calendarEntryAdded != null && (bool) !calendarEntryAdded) return;
            OnPropertyChanged(nameof(SelectedPerson));
        }

        public void EditCalendarEntryButtonClick()
        {
            var calendarEntrySaved = new EditCalendarEntryView(SelectedPerson, CalendarListSelectedIndex).ShowDialog();
            if (calendarEntrySaved != null && (bool) !calendarEntrySaved) return;
            OnPropertyChanged(nameof(SelectedPerson));
        }

        public void DeleteCalendarEntryButtonClick()
        {
            SelectedPerson.DeleteCalendarEntry(CalendarListSelectedIndex);
        }

        public void SaveAndClose(CancelEventArgs e)
        {
            var contactNoLastname = Contacts.ToList().Find(x => x.Lastname.Length == 0 && !x.Person.IsEmpty());
            if (contactNoLastname != null)
            {
                MessageBox.Show(
                    $"Achternaam mag niet leeg zijn!\nIn het contactenboek heeft de volgende persoon een lege achternaam:\nNaam:\t{contactNoLastname.Fullname}\nAdres:\t{contactNoLastname.Street} {contactNoLastname.HouseNumber}\n\t\t{contactNoLastname.Postalcode} {contactNoLastname.City}",
                    "Lege achternaam");
                e.Cancel = true;
                return;
            }
            Save();
        }

        private void Save()
        {
            Settings.Default.LastContacts = JsonConvert.SerializeObject(Contacts);

            _contactsSheet.CreateExcel(Contacts.ToList());

            var contactsCalendarEntries = Contacts.Select(x => x.CalendarEntries);
            var calendarEntries = new List<CalendarEntry>();
            foreach (var entries in contactsCalendarEntries)
            {
                calendarEntries.AddRange(entries);
            }

            _calendarSheet.CreateExcel(calendarEntries);
            ExcelFile.SaveNewPackage();
            
            ExcelFile.NewPackage = new ExcelPackage();
            LoadSheets(ContactsExcel, MessageText);
            LoadContacts();
        }
        public void SaveWithoutExit()
        {
            var contactNoLastname = Contacts.ToList().Find(x => x.Lastname.Length == 0 && !x.Person.IsEmpty());
            if (contactNoLastname != null)
            {
                MessageBox.Show(
                    $"Het bestand is niet opgeslagen!\nAchternaam mag niet leeg zijn!\nIn het contactenboek heeft de volgende persoon een lege achternaam:\nNaam:\t{contactNoLastname.Fullname}\nAdres:\t{contactNoLastname.Street} {contactNoLastname.HouseNumber}\n\t\t{contactNoLastname.Postalcode} {contactNoLastname.City}",
                    "Lege achternaam");
                return;
            }
            Save();
        }

        public void OpenFile()
        {
            ExcelFile.NewPackage = new ExcelPackage();
            LoadSheets(ContactsExcel, MessageText, true);
            LoadContacts();
        }
    }
}