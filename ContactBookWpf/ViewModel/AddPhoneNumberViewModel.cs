﻿using System;

namespace ContactBookWpf.ViewModel
{
    public class AddPhoneNumberViewModel : ViewModelBase
    {
        private string _introText;
        private PersonViewModel _selectedPerson;
        public string PhoneNumber { get; set; }

        public PersonViewModel SelectedPerson
        {
            get => _selectedPerson;
            set
            {
                SetProperty(ref _selectedPerson, value);
                IntroText =
                    $"Voer hieronder het telefoonnummer in wat u wilt toevoegen aan de onderstaande contactpersoon.{Environment.NewLine}" +
                    $"{Environment.NewLine}" +
                    $"Naam: {SelectedPerson.Fullname}{Environment.NewLine}" +
                    $"Adres: {SelectedPerson.Street} {SelectedPerson.HouseNumber}{Environment.NewLine}" +
                    $"Postcode: {SelectedPerson.Postalcode}{Environment.NewLine}" +
                    $"Woonplaats: {SelectedPerson.City}{Environment.NewLine}";
            }
        }

        public string IntroText
        {
            get => _introText;
            set => SetProperty(ref _introText, value);
        }

        public void AddPhoneNumber()
        {
            SelectedPerson.Person.AddPhoneNumber(PhoneNumber);
        }
    }
}