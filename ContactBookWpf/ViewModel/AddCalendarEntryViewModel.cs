﻿using System;

namespace ContactBookWpf.ViewModel
{
    public class AddCalendarEntryViewModel : ViewModelBase
    {
        private string _introText;
        private PersonViewModel _selectedPerson;

        public AddCalendarEntryViewModel()
        {
            Date = DateTime.Now;
        }

        public string Description { get; set; }
        public DateTime Date { get; set; }
        public bool EveryYear { get; set; }

        public PersonViewModel SelectedPerson
        {
            get => _selectedPerson;
            set
            {
                SetProperty(ref _selectedPerson, value);
                IntroText =
                    "Voer hieronder de datum en de beschrijving die bij de datum hoort in. " +
                    "Vink ieder jaar aan als het zich ieder jaar herhaald, zoals een verjaardag. " +
                    $"Dit item wordt toegevoegd aan de onderstaande contactpersoon{Environment.NewLine}" +
                    $"{Environment.NewLine}" +
                    $"Naam: {SelectedPerson.Fullname}{Environment.NewLine}" +
                    $"Adres: {SelectedPerson.Street} {SelectedPerson.HouseNumber}{Environment.NewLine}" +
                    $"Postcode: {SelectedPerson.Postalcode}{Environment.NewLine}" +
                    $"Woonplaats: {SelectedPerson.City}{Environment.NewLine}";
            }
        }

        public string IntroText
        {
            get => _introText;
            set => SetProperty(ref _introText, value);
        }

        public void AddCalendarEntry()
        {
            SelectedPerson.Person.AddCalendarEntry(Date, Description, EveryYear);
        }
    }
}