﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ContactBookWpf.Model
{
    public class Person
    {
        private const string NewContactText = "Nieuw contactpersoon";
        private static int _currentId = 1;

        public Person()
        {
            Id = _currentId;
            _currentId++;

            Firstname = string.Empty;
            Surname = string.Empty;
            Lastname = string.Empty;
            Street = string.Empty;
            HouseNumber = string.Empty;
            Postalcode = string.Empty;
            City = string.Empty;
            PhoneNumbers = new List<string>();
            CalendarEntries = new List<CalendarEntry>();
        }

        public Person(string lastname)
        {
            Id = _currentId;
            _currentId++;

            Firstname = string.Empty;
            Surname = string.Empty;
            Lastname = lastname;
            Street = string.Empty;
            HouseNumber = string.Empty;
            Postalcode = string.Empty;
            City = string.Empty;
            PhoneNumbers = new List<string>();
            CalendarEntries = new List<CalendarEntry>();
        }

        public int Id { get; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string Lastname { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string Postalcode { get; set; }
        public string City { get; set; }
        public List<string> PhoneNumbers { get; }
        public List<CalendarEntry> CalendarEntries { get; set; }

        // ReSharper disable once MemberCanBePrivate.Global
        public string Fullname =>
            IsEmpty() ? NewContactText : Regex.Replace($"{Firstname} {Surname} {Lastname}", @"\s+", " ").Trim();

        public void AddCalendarEntry(DateTime date, string description, bool everyYear = false)
        {
            if (string.IsNullOrEmpty(description)) return;
            CalendarEntries.Add(new CalendarEntry(Fullname, date, description, everyYear));
        }

        public void AddPhoneNumber(string phoneNumber)
        {
            if (phoneNumber.Length > 0)
                PhoneNumbers.Add(phoneNumber);
        }

        public bool IsEmpty()
        {
            if (Firstname.Length > 0)
                return false;
            if (Surname.Length > 0)
                return false;
            if (Lastname.Length > 0)
                return false;
            if (Street.Length > 0)
                return false;
            if (HouseNumber.Length > 0)
                return false;
            if (Postalcode.Length > 0)
                return false;
            if (City.Length > 0)
                return false;
            return PhoneNumbers.Count <= 0;
        }

        internal bool IsNew()
        {
            if (Firstname.Length > 0)
                return false;
            if (Surname.Length > 0)
                return false;
            if (Lastname.Equals(NewContactText) == false)
                return false;
            if (Street.Length > 0)
                return false;
            if (HouseNumber.Length > 0)
                return false;
            if (Postalcode.Length > 0)
                return false;
            if (City.Length > 0)
                return false;
            return PhoneNumbers.Count <= 0;
        }

        /// <summary>
        ///     Trims all the string attributes of the person, including the phone numbers and the calendar entries.
        /// </summary>
        public void Trim()
        {
            Firstname = Firstname.Trim();
            Surname = Surname.Trim();
            Lastname = Lastname.Trim();
            Street = Street.Trim();
            HouseNumber = HouseNumber.Trim();
            Postalcode = Postalcode.Trim();
            City = City.Trim();
            for (var i = 0; i < PhoneNumbers.Count; i++)
            {
                PhoneNumbers[i] = PhoneNumbers[i].Trim();
            }

            foreach (var entry in CalendarEntries)
            {
                entry.Description = entry.Description.Trim();
            }
        }
    }
}