﻿using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using OfficeOpenXml;

namespace ContactBookWpf.Model.Handler
{
    public class ExcelFile
    {
        private static FileInfo _fileInfo;
        public static ExcelPackage NewPackage = new ExcelPackage();

        public ExcelFile(string fileName)
        {
            if (!fileName.Contains(".xlsx"))
            {
                ConvertToXlsx(fileName);
                fileName += "x";
            }

            _fileInfo = new FileInfo(fileName);
            if (!_fileInfo.Exists) throw new FileNotFoundException();
        }

        public Dictionary<string, List<List<string>>> GetSheetsAsDict(BackgroundWorker worker)
        {
            var package = new ExcelPackage(_fileInfo);
            var worksheetCount = package.Workbook.Worksheets.Count;

            var sheets = new Dictionary<string, List<List<string>>>();
            var sheetsCompletion = new double[worksheetCount];

            var worksheets = package.Workbook.Worksheets.ToArray();

            Parallel.For((long) 0, worksheetCount, i =>
            {
                var data = new List<List<string>>();
                var worksheet = worksheets[i];

                if (worksheet.Hidden != eWorkSheetHidden.Visible)
                {
                    sheetsCompletion[i] = 100;
                    return;
                }

                var range = worksheet.Dimension;

                double max = range.Columns;
                for (var j = 1; j <= max; j++)
                {
                    var row = GetRowValues(worksheet, range, j);

                    if (!IsRowEmpty(row)) data.Add(row);
                    Parallel.For(0, row.Count, item =>
                    {
                        if (row[item] == null)
                            row[item] = "";
                    });

                    var percentage = j / max * 100;
                    sheetsCompletion[i] = percentage;
                    // worker.ReportProgress((int) sheetsCompletion.Average());
                }

                sheets.Add(worksheet.Name, data);
            });

            return sheets;
        }

        private static List<string> GetRowValues(ExcelWorksheet worksheet, ExcelAddressBase range, int column)
        {
            var rowValues = new string[range.Rows];

            Parallel.For(1, range.Rows + 1, i =>
            {
                var cell = worksheet.Cells[i, column];
                if (cell.Merge) return;

                //Add the string value if the cell value is not null
                rowValues[i - 1] = cell.Value?.ToString();
            });

            return new List<string>(rowValues);
        }

        private static bool IsRowEmpty(List<string> row)
        {
            return row.FindAll(x => x == null).Count == row.Count;
        }

        /// <summary>
        ///     Saves the new package and overwrites the old file if it exists
        /// </summary>
        public static void SaveNewPackage()
        {
            NewPackage.SaveAs(_fileInfo);
        }

        private static void ConvertToXlsx(string fileName)
        {
            var app = new Application();
            var wb = app.Workbooks.Open(fileName);
            wb.SaveAs(fileName + "x", XlFileFormat.xlOpenXMLWorkbook);
            wb.Close();
            app.Quit();
        }
    }
}