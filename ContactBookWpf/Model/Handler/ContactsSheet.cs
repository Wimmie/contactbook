﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using ContactBookWpf.ViewModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace ContactBookWpf.Model.Handler
{
    public class ContactsSheet : ContactBookSheet
    {
        private const int FirstNamePos = 1;
        private const int SurnamePos = 2;
        private const int LastnamePos = 3;
        private const int StreetPos = 4;
        private const int HouseNumberPos = 5;
        private const int PostalcodePos = 6;
        private const int CityPos = 7;
        private const int PhoneNumberPos = 8;

        public ContactsSheet(List<List<string>> data) : base(data)
        {
            WorkSheet = NewPackage.Workbook.Worksheets.Add(Name);
        }

        public static string Name => "Contacten";

        public List<PersonViewModel> GetContacts()
        {
            var contacts = new List<PersonViewModel>();

            // Start from 1 to skip the header row
            for (var row = 1; row < Data[0].Count; row++)
            {
                var lastnamePosValue = Data[LastnamePos - 1][row];
                if (lastnamePosValue == string.Empty) continue;

                var p = new Person(Data[LastnamePos - 1][row])
                {
                    Firstname = Data[FirstNamePos - 1][row],
                    Surname = Data[SurnamePos - 1][row],
                    Street = Data[StreetPos - 1][row],
                    HouseNumber = Data[HouseNumberPos - 1][row],
                    Postalcode = Data[PostalcodePos - 1][row],
                    City = Data[CityPos - 1][row],
                    CalendarEntries = new List<CalendarEntry>()
                };

                GetPhoneNumbers(p, row);
                contacts.Add(new PersonViewModel(p));
            }

            return contacts.OrderBy(x => x.Fullname).ToList();
        }

        /// <summary>
        /// Retrieves all the phone numbers of the contact out of the sheet
        /// </summary>
        /// <param name="p">The selected person</param>
        /// <param name="row">The row on which the information of the person can be found</param>
        private void GetPhoneNumbers(Person p, in int row)
        {
            p.AddPhoneNumber(Data[PhoneNumberPos - 1][row]);

            var phoneRow = row + 1;
            if (phoneRow >= Data[LastnamePos - 1].Count) return;
            var phoneRowLastnamePosValue = Data[LastnamePos - 1][phoneRow];
            var phoneRowPhoneNumberPosValue = Data[PhoneNumberPos - 1][phoneRow];

            while (phoneRowLastnamePosValue.Equals(string.Empty) && !phoneRowPhoneNumberPosValue.Equals(string.Empty) &&
                   phoneRow < Data[0].Count)
            {
                p.AddPhoneNumber(Data[PhoneNumberPos - 1][phoneRow]);
                phoneRow++;
                phoneRowLastnamePosValue = Data[LastnamePos - 1][phoneRow];
                phoneRowPhoneNumberPosValue = Data[PhoneNumberPos - 1][phoneRow];
            }
        }

        public void CreateExcel(List<PersonViewModel> items)
        {
            base.CreateExcel();
            items = SortObjects(items);

            var usedLetters = new List<char>();
            var row = 2;
            foreach (var person in items)
            {
                var lastNameFirstLetter = person.Lastname.ToUpper().First();
                if (!usedLetters.Contains(lastNameFirstLetter))
                {
                    usedLetters.Add(lastNameFirstLetter);

                    SetLastnameLetterRow(row, lastNameFirstLetter);

                    row++;
                }

                SetBorder(row);

                row = SetValues(person.Person, row);
            }
        }

        private int SetValues(Person person, int row)
        {
            person.Trim();
            if (person.PhoneNumbers.Count == 0)
            {
                SetCellValues(row, person);
                return row + 1;
            }

            for (var j = 0; j < person.PhoneNumbers.Count; j++)
            {
                if (j == 0)
                {
                    SetCellValuesWithPhoneNumber(row, person);
                }
                else
                {
                    SetBorder(row);

                    SetCellPhoneNumberValue(row, person, j);
                }

                row++;
            }

            return row;
        }

        private void SetCellPhoneNumberValue(int row, Person person, int j)
        {
            WorkSheet.Cells[row, PhoneNumberPos].Style.Numberformat.Format = "@";
            WorkSheet.Cells[row, PhoneNumberPos].Value = "" + person.PhoneNumbers[j];
        }

        private void SetCellValuesWithPhoneNumber(int row, Person person)
        {
            SetCellValues(row, person);
            WorkSheet.Cells[row, PhoneNumberPos].Style.Numberformat.Format = "@";
            WorkSheet.Cells[row, PhoneNumberPos].Value = "" + person.PhoneNumbers.First();
        }

        private void SetCellValues(int row, Person person)
        {
            WorkSheet.Cells[row, FirstNamePos].Value = person.Firstname;
            WorkSheet.Cells[row, SurnamePos].Value = person.Surname;
            WorkSheet.Cells[row, LastnamePos].Value = person.Lastname;
            WorkSheet.Cells[row, StreetPos].Value = person.Street;
            WorkSheet.Cells[row, HouseNumberPos].Style.Numberformat.Format = "@";
            WorkSheet.Cells[row, HouseNumberPos].Value = "" + person.HouseNumber;
            WorkSheet.Cells[row, PostalcodePos].Value = person.Postalcode;
            WorkSheet.Cells[row, CityPos].Value = person.City;
        }

        private void SetBorder(int row)
        {
            SetBottomBorderAndBorderColor(row, FirstNamePos, BorderLineStyle, BorderColor);
            SetBottomBorderAndBorderColor(row, SurnamePos, BorderLineStyle, BorderColor);
            SetBottomBorderAndBorderColor(row, LastnamePos, BorderLineStyle, BorderColor);
            SetBottomBorderAndBorderColor(row, StreetPos, BorderLineStyle, BorderColor);
            SetBottomBorderAndBorderColor(row, HouseNumberPos, BorderLineStyle, BorderColor);
            SetBottomBorderAndBorderColor(row, PostalcodePos, BorderLineStyle, BorderColor);
            SetBottomBorderAndBorderColor(row, CityPos, BorderLineStyle, BorderColor);
            SetBottomBorderAndBorderColor(row, PhoneNumberPos, BorderLineStyle, BorderColor);
        }

        private void SetLastnameLetterRow(int row, char lastnameFirstLetter)
        {
            var firstCell = WorkSheet.Cells[row, 1];
            var range = WorkSheet.Cells[row, 1, row, PhoneNumberPos];

            range.Merge = true;
            SetBottomBorderAndBorderColor(range, BorderLineStyle, BorderColor);
            range.Style.Fill.PatternType = ExcelFillStyle.Solid;
            range.Style.Fill.BackgroundColor.SetColor(Color.LightYellow);
            firstCell.Value = lastnameFirstLetter.ToString().ToUpper();
            firstCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        }

        protected override void InitHeader()
        {
            WorkSheet.PrinterSettings.RepeatRows = new ExcelAddress("$1:$1");

            StyleHeaderCell(FirstNamePos, "Voornaam");
            StyleHeaderCell(SurnamePos, "Tussenv.");
            StyleHeaderCell(LastnamePos, "Achternaam");
            StyleHeaderCell(StreetPos, "Straat");
            StyleHeaderCell(HouseNumberPos, "Huisnr.");
            StyleHeaderCell(PostalcodePos, "Postcode");
            StyleHeaderCell(CityPos, "Plaats");
            StyleHeaderCell(PhoneNumberPos, "Telefoonnummer");
        }

        protected override void SetWorksheetStyle()
        {
            WorkSheet.Column(FirstNamePos).Width = 12.5;
            WorkSheet.Column(SurnamePos).Width = 13.5;
            WorkSheet.Column(LastnamePos).Width = 17;
            WorkSheet.Column(StreetPos).Width = 20;
            WorkSheet.Column(HouseNumberPos).Width = 13;
            WorkSheet.Column(PostalcodePos).Width = 10;
            WorkSheet.Column(CityPos).Width = 11;
            WorkSheet.Column(PhoneNumberPos).Width = 16;

            base.SetWorksheetStyle();
        }

        private static List<PersonViewModel> SortObjects(List<PersonViewModel> contacts)
        {
            contacts.Remove(contacts.Find(x => x.Person.IsEmpty() || x.Person.IsNew()));
            return contacts.OrderBy(x => x.Lastname).ToList();
        }
    }
}