﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using ContactBookWpf.ViewModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace ContactBookWpf.Model.Handler
{
    public class CalendarSheet : ContactBookSheet
    {
        private const int ContactPos = 1;
        private const int DatePos = 2;
        private const int DescriptionPos = 3;

        public CalendarSheet(List<List<string>> data) : base(data)
        {
            WorkSheet = NewPackage.Workbook.Worksheets.Add(Name);
        }

        public static string Name => "Kalender";

        public void AddCalendarEntriesToContacts(List<PersonViewModel> contacts)
        {
            // Start from 1 to skip the header row
            for (var row = 1; row < Data[0].Count; row++)
            {
                var fullname = Data[ContactPos - 1][row];
                var dateAsString = Data[DatePos - 1][row];
                var description = Data[DescriptionPos - 1][row];
                var everyYear = false;

                if (fullname == string.Empty) continue;

                var dateParseSuccess = DateTime.TryParseExact(dateAsString, "dd-MM-yyyy", CultureInfo.InvariantCulture,
                    DateTimeStyles.None, out var date);

                // This happens when the date is not a string but a double or it is every year
                if (!dateParseSuccess)
                {
                    dateParseSuccess = DateTime.TryParseExact(dateAsString, "dd-MM", CultureInfo.InvariantCulture,
                        DateTimeStyles.None, out date);
                    everyYear = true;

                    // Date should be a double then
                    if (!dateParseSuccess)
                    {
                        date = DateTime.FromOADate(double.Parse(dateAsString));
                    }
                }

                var person = contacts.Find(x => x.Fullname == Regex.Replace(fullname, @"\s+", " "));
                person.Person.AddCalendarEntry(date, description, everyYear);
            }
        }

        public void CreateExcel(IEnumerable<CalendarEntry> items)
        {
            base.CreateExcel();

            var usedDates = new List<int>();
            var row = 2;
            foreach (var calendarEntry in items.OrderBy(x => x.Date.Month).ThenBy(x => x.Date.Day))
            {
                var month = calendarEntry.Date.Month;
                if (!usedDates.Contains(month))
                {
                    usedDates.Add(month);

                    var fullMonthName =
                        new DateTime(2015, month, 1).ToString("MMMM", CultureInfo.CreateSpecificCulture("nl"));
                    SetMonthRow(row, fullMonthName);

                    row++;
                }

                row = SetValues(calendarEntry, row);
            }
        }

        private void SetMonthRow(int row, string month)
        {
            var range = WorkSheet.Cells[row, 1, row, DescriptionPos];
            range.Merge = true;

            SetBottomBorderAndBorderColor(range, BorderLineStyle, BorderColor);
            range.Style.Fill.PatternType = ExcelFillStyle.Solid;
            range.Style.Fill.BackgroundColor.SetColor(Color.LightYellow);
            range.Value = month.First().ToString().ToUpper() + month.Substring(1);
            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        }

        private int SetValues(CalendarEntry calendarEntry, int row)
        {
            WorkSheet.Cells[row, ContactPos].Value = calendarEntry.Contact;
            var format = "dd-MM-yyyy";
            if (calendarEntry.EveryYear)
            {
                format = "dd-MM";
            }

            WorkSheet.Cells[row, DatePos].Value = calendarEntry.Date.ToString(format);
            WorkSheet.Cells[row, DescriptionPos].Value = calendarEntry.Description;
            return row + 1;
        }

        protected override void SetWorksheetStyle()
        {
            WorkSheet.Column(ContactPos).Width = 40;
            WorkSheet.Column(DatePos).Width = 13.5;
            WorkSheet.Column(DescriptionPos).Width = 40;

            base.SetWorksheetStyle();
        }


        protected override void InitHeader()
        {
            WorkSheet.PrinterSettings.RepeatRows = new ExcelAddress("$1:$1");

            StyleHeaderCell(ContactPos, "Persoon");
            StyleHeaderCell(DatePos, "Datum");
            StyleHeaderCell(DescriptionPos, "Beschrijving");
        }
    }
}