﻿using System.Collections.Generic;
using System.Drawing;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace ContactBookWpf.Model.Handler
{
    public abstract class ContactBookSheet
    {
        protected readonly List<List<string>> Data;
        protected Color BorderColor;
        protected readonly ExcelBorderStyle BorderLineStyle;
        private readonly Color _headerBackgroundColor;
        protected ExcelWorksheet WorkSheet;

        protected ContactBookSheet(List<List<string>> data)
        {
            Data = data;
            _headerBackgroundColor = Color.LightBlue;
            BorderColor = Color.LightGray;
            BorderLineStyle = ExcelBorderStyle.Thin;
        }

        protected static ExcelPackage NewPackage => ExcelFile.NewPackage;

        protected void SetBottomBorderAndBorderColor(int row, int cell, ExcelBorderStyle style, Color color)
        {
            SetBottomBorderAndBorderColor(WorkSheet.Cells[row, cell], style, color);
        }

        protected static void SetBottomBorderAndBorderColor(ExcelRange cell, ExcelBorderStyle style, Color color)
        {
            cell.Style.Border.Bottom.Style = style;
            cell.Style.Border.Bottom.Color.SetColor(color);
        }

        protected virtual void SetWorksheetStyle()
        {
            WorkSheet.PrinterSettings.Orientation = eOrientation.Landscape;
            WorkSheet.Cells.Style.Font.Size = 12;
        }

        protected void StyleHeaderCell(int cellNumber, string cellValue)
        {
            var cell = GetFirstRowCell(cellNumber);
            cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
            cell.Style.Fill.BackgroundColor.SetColor(_headerBackgroundColor);
            cell.Value = cellValue;
            SetBottomBorderAndBorderColor(cell, BorderLineStyle, BorderColor);
        }

        private ExcelRange GetFirstRowCell(int cellNumber)
        {
            return WorkSheet.Cells[1, cellNumber];
        }

        protected abstract void InitHeader();
        
        protected void CreateExcel()
        {
            SetWorksheetStyle();
            InitHeader();
        }
    }
}