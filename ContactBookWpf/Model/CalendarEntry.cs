﻿using System;

namespace ContactBookWpf.Model
{
    public class CalendarEntry
    {
        public CalendarEntry(string contact, DateTime date, string description, bool everyYear = false)
        {
            Contact = contact;
            Date = date;
            Description = description;
            EveryYear = everyYear;
        }

        public string Contact { get; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public bool EveryYear { get; set; }

        public string GetYear => EveryYear ? "Ieder jaar" : Date.Year.ToString();
    }
}