﻿using System.Windows;
using ContactBookWpf.ViewModel;

namespace ContactBookWpf.View
{
    public partial class EditPhoneNumberView
    {
        public EditPhoneNumberView(PersonViewModel selectedPerson, int phoneNumberIndex)
        {
            InitializeComponent();
            GetDataContext().SelectedPerson = selectedPerson;
            GetDataContext().PhoneNumberIndex = phoneNumberIndex;
        }

        private EditPhoneNumberViewModel GetDataContext()
        {
            return (EditPhoneNumberViewModel) DataContext;
        }

        private void SaveNumberButton_OnClick(object sender, RoutedEventArgs e)
        {
            GetDataContext().SavePhoneNumber();
            DialogResult = true;
        }
    }
}