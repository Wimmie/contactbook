﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using ContactBookWpf.ViewModel;

namespace ContactBookWpf.View
{
    public partial class ContactOverview
    {
        public ContactOverview()
        {
            InitializeComponent();
        }

        private ContactOverviewViewModel GetDataContext()
        {
            return (ContactOverviewViewModel) DataContext;
        }

        private void DeleteContact_OnClick(object sender, RoutedEventArgs e)
        {
            GetDataContext().DeleteContactButtonClick();
        }

        private void DeletePhoneNumber_OnClick(object sender, RoutedEventArgs e)
        {
            GetDataContext().DeletePhoneNumberButtonClick();
        }

        private void AddPhoneNumber_OnClick(object sender, RoutedEventArgs e)
        {
            GetDataContext().AddPhoneNumberButtonClick();
        }

        private void EditPhoneNumber_OnClick(object sender, MouseButtonEventArgs e)
        {
            GetDataContext().EditPhoneNumberButtonClick();
        }

        private void DeleteCalendarEntry_OnClick(object sender, RoutedEventArgs e)
        {
            GetDataContext().DeleteCalendarEntryButtonClick();
        }

        private void AddCalendarEntry_OnClick(object sender, RoutedEventArgs e)
        {
            GetDataContext().AddCalendarEntryButtonClick();
        }

        private void EditCalendarEntry_OnClick(object sender, MouseButtonEventArgs e)
        {
            GetDataContext().EditCalendarEntryButtonClick();
        }

        private void ContactOverview_OnClosing(object sender, CancelEventArgs e)
        {
            GetDataContext().SaveAndClose(e);
        }

        private void CloseAndSaveButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OpenFile_OnClick(object sender, RoutedEventArgs e)
        {
            GetDataContext().OpenFile();
        }

        private void Save_OnClick(object sender, RoutedEventArgs e)
        {
            GetDataContext().SaveWithoutExit();
        }
    }
}