﻿using System.Windows;
using ContactBookWpf.ViewModel;

namespace ContactBookWpf.View
{
    public partial class AddCalendarEntryView
    {
        public AddCalendarEntryView(PersonViewModel selectedPerson)
        {
            InitializeComponent();
            GetDataContext().SelectedPerson = selectedPerson;
        }

        private AddCalendarEntryViewModel GetDataContext()
        {
            return (AddCalendarEntryViewModel) DataContext;
        }

        private void AddCalenderEntryButton_OnClick(object sender, RoutedEventArgs e)
        {
            GetDataContext().AddCalendarEntry();
            DialogResult = true;
        }
    }
}