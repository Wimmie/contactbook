﻿using System.Windows;
using ContactBookWpf.ViewModel;

namespace ContactBookWpf.View
{
    public partial class AddPhoneNumberView
    {
        public AddPhoneNumberView(PersonViewModel selectedPerson)
        {
            InitializeComponent();
            GetDataContext().SelectedPerson = selectedPerson;
        }

        private AddPhoneNumberViewModel GetDataContext()
        {
            return (AddPhoneNumberViewModel) DataContext;
        }

        private void AddNumberButton_OnClick(object sender, RoutedEventArgs e)
        {
            GetDataContext().AddPhoneNumber();
            DialogResult = true;
        }
    }
}