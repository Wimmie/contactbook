﻿using System.Windows;
using ContactBookWpf.ViewModel;

namespace ContactBookWpf.View
{
    public partial class EditCalendarEntryView
    {
        public EditCalendarEntryView(PersonViewModel selectedPerson, int calendarEntryIndex)
        {
            InitializeComponent();
            GetDataContext().SelectedPerson = selectedPerson;
            GetDataContext().CalendarEntry = selectedPerson.Person.CalendarEntries[calendarEntryIndex];
        }

        private EditCalendarEntryViewModel GetDataContext()
        {
            return (EditCalendarEntryViewModel) DataContext;
        }

        private void SaveCalenderEntryButton_OnClick(object sender, RoutedEventArgs e)
        {
            GetDataContext().SaveCalendarEntry();
            DialogResult = true;
        }
    }
}