﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using NetOffice.ExcelApi.Enums;
using NetOffice.Exceptions;

namespace ContactBook.Classes.Handler
{
    public class CalendarWorksheet : ContactBookWorkbook<CalendarEntry>
    {
        private int ContactPos { get; set; }
        private int DatePos { get; set; }
        private int DescriptionPos { get; set; }

        public CalendarWorksheet(): base("Kalender")
        {
        }

        public CalendarWorksheet(string workbook) : base("Kalender", workbook)
        {
        }
        protected override void InitPositions()
        {
            ContactPos = 1;
            DatePos = 2;
            DescriptionPos = 3;
        }

        protected override void InitHeader()
        {
            Worksheet.PageSetup.PrintTitleRows = "$1:$1";

            StyleHeaderCell(ContactPos, "Persoon");
            StyleHeaderCell(DatePos, "Datum");
            StyleHeaderCell(DescriptionPos, "Beschrijving");
        }

        protected override List<CalendarEntry> SortObjects(List<CalendarEntry> calendarEntries)
        {
            return calendarEntries.OrderBy(x => x.Date).ToList();
        }

        protected override void SetWorksheetStyle()
        {
            Worksheet.PageSetup.Orientation = XlPageOrientation.xlLandscape;

            Worksheet.Columns[ContactPos].ColumnWidth = 35;
            Worksheet.Columns[DatePos].ColumnWidth = 13.5;
            Worksheet.Columns[DescriptionPos].ColumnWidth = 40;

            base.SetWorksheetStyle();
        }

        protected override void FillSheetWithObjects(IEnumerable<CalendarEntry> calendarEntries)
        {
            var usedDates = new List<int>();
            var row = 2;
            foreach (var calendarEntry in calendarEntries)
            {
                var date = calendarEntry.Date;
                if (!usedDates.Contains(date.Month))
                {
                    usedDates.Add(date.Month);

                    var fullMonthName = new DateTime(2015, date.Month, 1).ToString("MMMM", CultureInfo.CreateSpecificCulture("nl"));
                    SetMonthRow(row, fullMonthName);

                    row++;
                }

                row = SetValues(calendarEntry, row);
            }
        }

        private void SetMonthRow(int row, string month)
        {
            var firstCell = Worksheet.Rows[row].Cells[1];
            var lastCell = Worksheet.Rows[row].Cells[DescriptionPos];
            var range = Worksheet.Range(firstCell, lastCell);

            range.Merge();
            SetBottomBorderAndBorderColor(range, BorderLineStyle, BorderColor);
            range.Interior.Color = Color.LightYellow;
            firstCell.Value2 = month.First().ToString().ToUpper() + month.Substring(1);
            firstCell.HorizontalAlignment = XlHAlign.xlHAlignCenter;
        }

        private int SetValues(CalendarEntry calendarEntry, int row)
        {
            Worksheet.Rows[row].Cells[ContactPos].Value2 = calendarEntry.Contact;
            Worksheet.Rows[row].Cells[DatePos].Value2 = calendarEntry.Date;
            Worksheet.Rows[row].Cells[DatePos].NumberFormat = "dd-MM-yyyy";
            Worksheet.Rows[row].Cells[DescriptionPos].Value2 = calendarEntry.Description;
            return row + 1;
        }

        public void AddCalendarEntriesToContacts(List<Person> contacts)
        {
            OpenWorkbookAndWorksheet();

            try
            {
                Parallel.For(0, Worksheet.UsedRange.Count, i =>
                {
                    var row = Worksheet.Rows[i + 1];
                    var firstCells = Worksheet.Range(row.Cells[1], row.Cells[2]);

                    var fullname = GetValueOrEmptyString(row.Cells[ContactPos]);
                    var dateAsString = GetValueOrEmptyString(row.Cells[DatePos]);
                    var description = GetValueOrEmptyString(row.Cells[DescriptionPos]);

                    if ((bool)firstCells.MergeCells || fullname == String.Empty || fullname == "Persoon") return;

                    var date = DateTime.FromOADate(double.Parse(dateAsString));

                    var person = contacts.Find(x => x.Fullname == fullname);
                    person.AddCalendarEntry(date, description);
                });
            }
            catch (PropertyGetCOMException)
            {
            }

            CloseFile();
        }
    }
}