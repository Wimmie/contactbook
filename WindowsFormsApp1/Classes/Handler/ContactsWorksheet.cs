﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using NetOffice.ExcelApi;
using NetOffice.ExcelApi.Enums;
using NetOffice.Exceptions;
using XlHAlign = NetOffice.OfficeApi.Enums.XlHAlign;

namespace ContactBook.Classes.Handler
{
    internal class ContactsWorksheet : ContactBookWorkbook<Person>
    {
        private int LastnamePos { get; set; }
        private int SurnamePos { get; set; }
        private int FirstNamePos { get; set; }
        private int StreetPos { get; set; }
        private int HouseNumberPos { get; set; }
        private int PostalcodePos { get; set; }
        private int CityPos { get; set; }
        private int PhoneNumberPos { get; set; }

        public ContactsWorksheet() : base("Contacten")
        {
        }

        public ContactsWorksheet(string workbook) : base("Contacten", workbook)
        {
        }

        protected override void InitPositions()
        {
            FirstNamePos = 1;
            SurnamePos = 2;
            LastnamePos = 3;
            StreetPos = 4;
            HouseNumberPos = 5;
            PostalcodePos = 6;
            CityPos = 7;
            PhoneNumberPos = 8;
        }

        public List<Person> GetContacts()
        {
            var contacts = new List<Person>();
            OpenWorkbookAndWorksheet();

            try
            {
                Parallel.For(0, Worksheet.UsedRange.Count, i =>
                {
                    var row = Worksheet.Rows[i + 1];
                    var firstCells = Worksheet.Range(row.Cells[1], row.Cells[2]);

                    if ((bool) firstCells.MergeCells || row.Cells[LastnamePos].Value2 == null ||
                        row.Cells[LastnamePos].Value2.Equals("Achternaam")) return;

                    var p = new Person(row.Cells[LastnamePos].Value2.ToString())
                    {
                        Firstname = GetValueOrEmptyString(row.Cells[FirstNamePos]),
                        Surname = GetValueOrEmptyString(row.Cells[SurnamePos]),
                        Street = GetValueOrEmptyString(row.Cells[StreetPos]),
                        HouseNumber = GetValueOrEmptyString(row.Cells[HouseNumberPos]),
                        Postalcode = GetValueOrEmptyString(row.Cells[PostalcodePos]),
                        City = GetValueOrEmptyString(row.Cells[CityPos]),
                        CalendarEntries = new List<CalendarEntry>()
                    };
                    p.AddPhoneNumber(GetValueOrEmptyString(row.Cells[PhoneNumberPos]));

                    var phoneRow = Worksheet.Rows[row.Row + 1];
                    while (phoneRow.Cells[LastnamePos].Value2 == null && phoneRow.Cells[PhoneNumberPos].Value2 != null)
                    {
                        p.AddPhoneNumber(phoneRow.Cells[PhoneNumberPos].Value2.ToString());
                        phoneRow = Worksheet.Rows[phoneRow.Row + 1];
                    }

                    contacts.Add(p);
                });
            }
            catch (PropertyGetCOMException)
            {
            }

            CloseFile();

            return contacts.OrderBy(x => x.Fullname).ToList();
        }

        protected override void FillSheetWithObjects(IEnumerable<Person> contacts)
        {
            var usedLetters = new List<char>();
            var row = 2;
            foreach (var person in contacts)
            {
                var lastNameFirstLetter = person.Lastname[0];
                if (!usedLetters.Contains(lastNameFirstLetter))
                {
                    usedLetters.Add(lastNameFirstLetter);

                    SetLastnameLetterRow(row, lastNameFirstLetter);

                    row++;
                }

                SetBorder(row);

                row = SetValues(person, row);
            }
        }

        private int SetValues(Person person, int row)
        {
            person.Trim();
            if (person.PhoneNumbers.Count == 0)
            {
                SetCellValues(row, person);
                return row + 1;
            }

            for (var j = 0; j < person.PhoneNumbers.Count; j++)
            {
                if (j == 0)
                {
                    SetCellValuesWithPhoneNumber(row, person);
                }
                else
                {
                    SetBorder(row);

                    SetCellPhoneNumberValue(row, person, j);
                }

                row++;
            }

            return row;
        }

        private void SetCellPhoneNumberValue(int row, Person person, int j)
        {
            Worksheet.Rows[row].Cells[PhoneNumberPos].Value2 = "'" + person.PhoneNumbers[j];
        }

        private void SetCellValuesWithPhoneNumber(int row, Person person)
        {
            Worksheet.Rows[row].Cells[FirstNamePos].Value2 = person.Firstname;
            Worksheet.Rows[row].Cells[SurnamePos].Value2 = person.Surname;
            Worksheet.Rows[row].Cells[LastnamePos].Value2 = person.Lastname;
            Worksheet.Rows[row].Cells[StreetPos].Value2 = person.Street;
            Worksheet.Rows[row].Cells[HouseNumberPos].Value2 = "'" + person.HouseNumber;
            Worksheet.Rows[row].Cells[PostalcodePos].Value2 = person.Postalcode;
            Worksheet.Rows[row].Cells[CityPos].Value2 = person.City;
            Worksheet.Rows[row].Cells[PhoneNumberPos].Value2 = "'" + person.PhoneNumbers.First();
        }

        private void SetCellValues(int row, Person person)
        {
            Worksheet.Rows[row].Cells[FirstNamePos].Value2 = person.Firstname;
            Worksheet.Rows[row].Cells[SurnamePos].Value2 = person.Surname;
            Worksheet.Rows[row].Cells[LastnamePos].Value2 = person.Lastname;
            Worksheet.Rows[row].Cells[StreetPos].Value2 = person.Street;
            Worksheet.Rows[row].Cells[HouseNumberPos].Value2 = "'" + person.HouseNumber;
            Worksheet.Rows[row].Cells[PostalcodePos].Value2 = person.Postalcode;
            Worksheet.Rows[row].Cells[CityPos].Value2 = person.City;
        }

        private void SetBorder(int row)
        {
            SetBottomBorderAndBorderColor(row, FirstNamePos, BorderLineStyle, BorderColor);
            SetBottomBorderAndBorderColor(row, SurnamePos, BorderLineStyle, BorderColor);
            SetBottomBorderAndBorderColor(row, LastnamePos, BorderLineStyle, BorderColor);
            SetBottomBorderAndBorderColor(row, StreetPos, BorderLineStyle, BorderColor);
            SetBottomBorderAndBorderColor(row, HouseNumberPos, BorderLineStyle, BorderColor);
            SetBottomBorderAndBorderColor(row, PostalcodePos, BorderLineStyle, BorderColor);
            SetBottomBorderAndBorderColor(row, CityPos, BorderLineStyle, BorderColor);
            SetBottomBorderAndBorderColor(row, PhoneNumberPos, BorderLineStyle, BorderColor);
        }

        private void SetLastnameLetterRow(int row, char lastnameFirstLetter)
        {
            var firstCell = Worksheet.Rows[row].Cells[1];
            var lastCell = Worksheet.Rows[row].Cells[PhoneNumberPos];
            var range = Worksheet.Range(firstCell, lastCell);

            range.Merge();
            SetBottomBorderAndBorderColor(range, BorderLineStyle, BorderColor);
            range.Interior.Color = Color.LightYellow;
            firstCell.Value2 = lastnameFirstLetter.ToString().ToUpper();
            firstCell.HorizontalAlignment = XlHAlign.xlHAlignCenter;
        }

        protected override List<Person> SortObjects(List<Person> contacts)
        {
            contacts.Remove(contacts.Find(x => x.IsEmpty() || x.IsNew()));
            return contacts.OrderBy(x => x.Lastname).ToList();
        }

        protected override void SetWorksheetStyle()
        {
            Worksheet.PageSetup.Orientation = XlPageOrientation.xlLandscape;

            Worksheet.Columns[FirstNamePos].ColumnWidth = 12.5;
            Worksheet.Columns[SurnamePos].ColumnWidth = 13.5;
            Worksheet.Columns[LastnamePos].ColumnWidth = 17;
            Worksheet.Columns[StreetPos].ColumnWidth = 20;
            Worksheet.Columns[HouseNumberPos].ColumnWidth = 13;
            Worksheet.Columns[PostalcodePos].ColumnWidth = 10;
            Worksheet.Columns[CityPos].ColumnWidth = 11;
            Worksheet.Columns[PhoneNumberPos].ColumnWidth = 16;

            base.SetWorksheetStyle();
        }

        protected override void InitHeader()
        {
            Worksheet.PageSetup.PrintTitleRows = "$1:$1";

            StyleHeaderCell(FirstNamePos, "Voornaam");
            StyleHeaderCell(SurnamePos, "Tussenv.");
            StyleHeaderCell(LastnamePos, "Achternaam");
            StyleHeaderCell(StreetPos, "Straat");
            StyleHeaderCell(HouseNumberPos, "Huisnr.");
            StyleHeaderCell(PostalcodePos, "Postcode");
            StyleHeaderCell(CityPos, "Plaats");
            StyleHeaderCell(PhoneNumberPos, "Telefoonnummer");
        }
    }
}