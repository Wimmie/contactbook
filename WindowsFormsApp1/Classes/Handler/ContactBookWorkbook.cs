﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using NetOffice.ExcelApi;
using NetOffice.ExcelApi.Enums;
using NetOffice.Exceptions;
using NetOffice.Extensions;
using Color = System.Drawing.Color;
using Workbook = NetOffice.ExcelApi.Workbook;
using Worksheet = NetOffice.ExcelApi.Worksheet;

namespace ContactBook.Classes.Handler
{
    public abstract class ContactBookWorkbook<T>
    {
        protected XlLineStyle BorderLineStyle;
        protected Color BorderColor;
        protected Color HeaderBackgroundColor;
        protected Workbook Workbook { get; set; }
        protected Worksheet Worksheet { get; set; }
        protected string SheetName { get; set; }
        private Application _excelApp;
        private readonly String _filename;

        protected ContactBookWorkbook(string sheetName)
        {
            SheetName = sheetName;
            _excelApp = new Application {DisplayAlerts = false};
            _filename = $@"{Directory.GetCurrentDirectory()}\Contacten2.xlsx";

            Workbook = _excelApp.Workbooks.Add();

            AddWorksheet();
            Workbook.SaveAs(_filename);
            CloseFile();
            InitPositions();
            HeaderBackgroundColor = Color.LightBlue;
            BorderColor = Color.LightGray;
            BorderLineStyle = XlLineStyle.xlContinuous;
        }

        protected ContactBookWorkbook(string sheetName, string workbook)
        {
            SheetName = sheetName;
            _excelApp = new Application();
            _filename = workbook;

            OpenWorkbookAndWorksheet();

            InitPositions();
            HeaderBackgroundColor = Color.LightBlue;
            BorderColor = Color.LightGray;
            BorderLineStyle = XlLineStyle.xlContinuous;
            
            CloseFile();
        }

        protected void OpenWorkbookAndWorksheet()
        {
            _excelApp = new Application();
            Workbook = _excelApp.Workbooks.Open(_filename);

            try
            {
                Worksheet = (Worksheet) Workbook.Sheets.First(x => ((Worksheet) x).Name == SheetName);
            }
            catch (InvalidOperationException)
            {
                AddWorksheet();
            }
        }

        private void AddWorksheet()
        {
            Worksheet = (Worksheet) Workbook.Worksheets.Add();
            Worksheet.Name = SheetName;
        }

        protected abstract void InitPositions();

        protected abstract void InitHeader();

        public void CreateExcelOfObjects(List<T> objects)
        {
            objects = SortObjects(objects);

            OpenWorkbookAndWorksheet();

            try
            {
                Worksheet.Cells.Clear();
            }
            catch (PropertyGetCOMException)
            {
            }

            SetWorksheetStyle();

            InitHeader();

            FillSheetWithObjects(objects);
            
            CloseFile();
        }

        protected void CloseFile()
        {
            try
            {
                ((Worksheet)Workbook.Worksheets.First(x => ((Worksheet)x).Name == "Blad1")).Delete();
            }
            catch (Exception)
            {
                // ignored
            }

            _excelApp.DisplayAlerts = false;
            Workbook.Close(1);
            _excelApp.Quit();
            _excelApp.Dispose();
        }

        protected abstract List<T> SortObjects(List<T> objects);

        protected virtual void SetWorksheetStyle()
        {
            Worksheet.UsedRange.Font.Size = 12;
        }

        protected abstract void FillSheetWithObjects(IEnumerable<T> objects);

        protected void StyleHeaderCell(int cellNumber, string cellValue)
        {
            var cell = GetFirstRowCell(cellNumber);
            cell.Interior.Color = HeaderBackgroundColor;
            cell.Value = cellValue;
            SetBottomBorderAndBorderColor(cell, BorderLineStyle, BorderColor);
        }

        private Range GetFirstRowCell(int cellNumber)
        {
            return Worksheet.Rows[1].Cells[cellNumber];
        }

        protected void SetBottomBorderAndBorderColor(int row, int cell, XlLineStyle style, Color color)
        {
            SetBottomBorderAndBorderColor(Worksheet.Rows[row].Cells[cell], style, color);
        }

        protected static void SetBottomBorderAndBorderColor(Range cell, XlLineStyle style, Color color)
        {
            cell.Borders.LineStyle = style;
            cell.Borders.Color = color;
        }

        protected static string GetValueOrEmptyString(Range cell)
        {
            return cell.Value2 == null ? "" : cell.Value2.ToString();
        }
    }
}