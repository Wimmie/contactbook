﻿using System;

namespace ContactBook.Classes
{
    public class CalendarEntry
    {
        public string Contact { get; }
        public DateTime Date;
        public string Description;

        public string EntryAsString => Date.ToString("dd-MM-yyyy") + " " + Description;

        public CalendarEntry(string contact, DateTime date, string description)
        {
            Contact = contact;
            Date = date;
            Description = description;
        }
    }
}