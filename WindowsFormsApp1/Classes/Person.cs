﻿using System;
using System.Collections.Generic;

namespace ContactBook.Classes
{
    public class Person
    {
        private const string NewContactText = "Nieuw contactpersoon";
        public static int CurrentId = 1;

        public int Id { get; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string Lastname { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string Postalcode { get; set; }
        public string City { get; set; }
        public List<string> PhoneNumbers { get; }
        public List<CalendarEntry> CalendarEntries { get; set; }
        public string Fullname => IsEmpty() ? NewContactText : $"{Firstname} {Surname} {Lastname}".Trim();

        public Person()
        {
            Id = CurrentId;
            CurrentId++;

            Firstname = "";
            Surname = "";
            Lastname = "";
            Street = "";
            HouseNumber = "";
            Postalcode = "";
            City = "";
            PhoneNumbers = new List<string>();
            CalendarEntries = new List<CalendarEntry>();
        }

        public Person(string lastname)
        {
            Id = CurrentId;
            CurrentId++;

            Firstname = "";
            Surname = "";
            Lastname = lastname;
            Street = "";
            HouseNumber = "";
            Postalcode = "";
            City = "";
            PhoneNumbers = new List<string>();
            CalendarEntries = new List<CalendarEntry>();
        }

        public void AddCalendarEntry(DateTime date, string description)
        {
            if (string.IsNullOrEmpty(description)) return;
            CalendarEntries.Add(new CalendarEntry(Fullname, date, description));
        }

        public void AddPhoneNumber(string phoneNumber)
        {
            if (phoneNumber.Length > 0)
                PhoneNumbers.Add(phoneNumber);
        }

        public void DeletePhoneNumber(string phoneNumber)
        {
            var index = PhoneNumbers.FindIndex(x => x == phoneNumber);
            PhoneNumbers.RemoveAt(index);
        }

        public bool IsEmpty()
        {
            if (Firstname.Length > 0)
                return false;
            if (Surname.Length > 0)
                return false;
            if (Lastname.Length > 0)
                return false;
            if (Street.Length > 0)
                return false;
            if (HouseNumber.Length > 0)
                return false;
            if (Postalcode.Length > 0)
                return false;
            if (City.Length > 0)
                return false;
            return PhoneNumbers.Count <= 0;
        }

        internal bool IsNew()
        {
            if (Firstname.Length > 0)
                return false;
            if (Surname.Length > 0)
                return false;
            if (Lastname.Equals(NewContactText) == false)
                return false;
            if (Street.Length > 0)
                return false;
            if (HouseNumber.Length > 0)
                return false;
            if (Postalcode.Length > 0)
                return false;
            if (City.Length > 0)
                return false;
            return PhoneNumbers.Count <= 0;
        }

        /// <summary>
        ///     Trims all the string attributes of the person, including the phone numbers and the calendar entries.
        /// </summary>
        public void Trim()
        {
            Firstname = Firstname.Trim();
            Surname = Surname.Trim();
            Lastname = Lastname.Trim();
            Street = Street.Trim();
            HouseNumber = HouseNumber.Trim();
            Postalcode = Postalcode.Trim();
            City = City.Trim();
            for (var i = 0; i < PhoneNumbers.Count; i++)
            {
                PhoneNumbers[i] = PhoneNumbers[i].Trim();
            }
            foreach (var entry in CalendarEntries)
            {
                entry.Description = entry.Description.Trim();
            }
        }
    }
}