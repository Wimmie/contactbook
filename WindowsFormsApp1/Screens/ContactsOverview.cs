﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;
using Contactbook.Screens;
using ContactBook.Classes;
using ContactBook.Classes.Handler;
using ContactBook.Properties;
using Newtonsoft.Json;

namespace ContactBook.Screens
{
    public partial class ContactsOverview : Form
    {
        private readonly string _excelFiles;
        private List<Person> _contacts;
        private ContactsWorksheet _contactsWorksheet;
        private CalendarWorksheet _calendarWorksheet;
        private Person _selectedPerson;

        public ContactsOverview()
        {
            InitializeComponent();

            // ReSharper disable once VirtualMemberCallInConstructor
            Text = Resources.ContactsOverview;

            var contactenExcel = "Contacten Excel";
            var messageText =
                "Selecteer een Excel bestand met daarin contacten. Klik op 'Oke' om door te gaan of op 'Annuleren' om een leeg werkblad te maken.";
            _excelFiles = "Excel Files|*.xls;*.xlsx;*.xlsm";

            SetContactsWorkbook(contactenExcel, messageText);

            LoadContacts();
        }

        private void SetContactsWorkbook(string messageCaption, string messageText)
        {
            TryOpenSettingsWorkbook();

            while (_contactsWorksheet == null)
            {
                var response = MessageBox.Show(
                    messageText,
                    messageCaption,
                    MessageBoxButtons.OKCancel);

                if (response == DialogResult.OK)
                {
                    OpenExcelDialog(_excelFiles);
                }
                else
                {
                    _contactsWorksheet = new ContactsWorksheet();
                    _calendarWorksheet = new CalendarWorksheet();
                    Settings.Default.Workbook = "Contacten.xlsx";
                }
            }

            Settings.Default.Save();
        }

        private void OpenExcelDialog(string fileFilter)
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = fileFilter
            };

            var userClicked = openFileDialog.ShowDialog();

            if (userClicked != DialogResult.OK) return;

            _contactsWorksheet = new ContactsWorksheet(openFileDialog.FileName);
            _calendarWorksheet = new CalendarWorksheet(openFileDialog.FileName);
            Settings.Default.Workbook = openFileDialog.FileName;
        }

        private void TryOpenSettingsWorkbook()
        {
            if (Settings.Default.Workbook.Length <= 0) return;
            try
            {
                _contactsWorksheet = new ContactsWorksheet(Settings.Default.Workbook);
                _calendarWorksheet = new CalendarWorksheet(Settings.Default.Workbook);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void LoadContacts()
        {
            _contacts = _contactsWorksheet.GetContacts();
            _calendarWorksheet.AddCalendarEntriesToContacts(_contacts);

            LoadContactList();
        }

        private void LoadContactList()
        {
            // Checks if an empty person should be added to the contacts list or removed.
            CheckEmptyPerson();

            contactsList.DataSource = _contacts;
            contactsList.DisplayMember = "";
            contactsList.DisplayMember = "Fullname";
        }

        private void CheckEmptyPerson()
        {
            var emptyPerson = _contacts.FindAll(x => x.IsEmpty());

            if (emptyPerson.Count == 0)
                _contacts.Add(new Person());
            else if (emptyPerson.Count > 1 && _selectedPerson.Id != emptyPerson.Last().Id)
                _contacts.Remove(emptyPerson.Last());
        }

        private void ContactsListSelectedIndexChanged(object sender, EventArgs e)
        {
            _selectedPerson = _contacts.Find(x => x == (Person) contactsList.SelectedItem);

            LoadFields();
        }

        private void LoadFields()
        {
            firstnameTextBox.Text = _selectedPerson.Firstname;
            surnameText.Text = _selectedPerson.Surname;
            lastnameTextBox.Text = _selectedPerson.Lastname;
            streetTextBox.Text = _selectedPerson.Street;
            houseNumberTextBox.Text = _selectedPerson.HouseNumber;
            postalcodeTextBox.Text = _selectedPerson.Postalcode;
            cityTextBox.Text = _selectedPerson.City;

            phoneNumbersList.DataSource = _selectedPerson.PhoneNumbers;
        }

        private void FirstnameTextBoxTextChanged(object sender, EventArgs e)
        {
            _selectedPerson.Firstname = firstnameTextBox.Text;
            LoadContactList();
        }

        private void LastnameTextBoxTextChanged(object sender, EventArgs e)
        {
            _selectedPerson.Lastname = lastnameTextBox.Text;
            LoadContactList();
        }

        private void StreetTextBoxTextChanged(object sender, EventArgs e)
        {
            _selectedPerson.Street = streetTextBox.Text;
            LoadContactList();
        }

        private void HouseNumberTextBoxTextChanged(object sender, EventArgs e)
        {
            _selectedPerson.HouseNumber = houseNumberTextBox.Text;
            LoadContactList();
        }

        private void PostalcodeTextBoxTextChanged(object sender, EventArgs e)
        {
            _selectedPerson.Postalcode = postalcodeTextBox.Text;
            LoadContactList();
        }

        private void CityTextBoxTextChanged(object sender, EventArgs e)
        {
            _selectedPerson.City = cityTextBox.Text;
            LoadContactList();
        }

        private void DeletePhoneNumberButtonClick(object sender, EventArgs e)
        {
            _selectedPerson.DeletePhoneNumber(phoneNumbersList.SelectedValue.ToString());
            LoadPhoneNumbersList();
        }

        private void LoadPhoneNumbersList()
        {
            phoneNumbersList.DataSource = new List<string>();
            phoneNumbersList.DataSource = _selectedPerson.PhoneNumbers;
        }

        private void AddPhoneNumberButtonClick(object sender, EventArgs e)
        {
            new AddPhoneNumber(_selectedPerson).ShowDialog();
        }

        private void ContactsOverview_Activated(object sender, EventArgs e)
        {
            if (_selectedPerson != null)
                LoadPhoneNumbersList();
        }

        private void ContactsOverview_FormClosing(object sender, FormClosingEventArgs e)
        {
            var contactNoLastname = _contacts.Find(x => x.Lastname.Length == 0 && !x.IsEmpty());
            if (contactNoLastname != null)
            {
                MessageBox.Show(
                    $"Achternaam mag niet leeg zijn!\nIn het contactenboek heeft de volgende persoon een lege achternaam:\nNaam:\t{contactNoLastname.Fullname}\nAdres:\t{contactNoLastname.Street} {contactNoLastname.HouseNumber}\n\t\t{contactNoLastname.Postalcode} {contactNoLastname.City}",
                    "Lege achternaam");
                e.Cancel = true;
                return;
            }

            Settings.Default.LastContacts = JsonConvert.SerializeObject(_contacts);

            _contactsWorksheet.CreateExcelOfObjects(_contacts);

            var contactsCalendarEntries = _contacts.Select(x => x.CalendarEntries);
            var calendarEntries = new List<CalendarEntry>();
            foreach (var entries in contactsCalendarEntries)
            {
                calendarEntries.AddRange(entries);
            }

            _calendarWorksheet.CreateExcelOfObjects(calendarEntries);
        }

        private void SurnameTextTextChanged(object sender, EventArgs e)
        {
            _selectedPerson.Surname = surnameText.Text;
            LoadContactList();
        }

        private void DeleteContactButtonClick(object sender, EventArgs e)
        {
            _contacts.Remove(_selectedPerson);
            LoadContactList();
        }

        private void CalenderButton_Click(object sender, EventArgs e)
        {
            new Calendar(_selectedPerson).ShowDialog();
        }
    }
}