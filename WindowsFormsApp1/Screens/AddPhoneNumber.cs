﻿using System;
using System.Windows.Forms;
using ContactBook.Classes;

namespace Contactbook.Screens
{
    public partial class AddPhoneNumber : Form
    {
        private readonly Person _selectedPerson;

        public AddPhoneNumber(Person selectedPerson)
        {
            _selectedPerson = selectedPerson;
            InitializeComponent();
        }

        private void AddButtonClick(object sender, EventArgs e)
        {
            _selectedPerson.AddPhoneNumber(phoneNumberTextBox.Text);
            Close();
        }

        private void CancelButtonClick(object sender, EventArgs e)
        {
            Close();
        }
    }
}