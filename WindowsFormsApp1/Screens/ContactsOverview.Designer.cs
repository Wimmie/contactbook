﻿namespace ContactBook.Screens
{
    partial class ContactsOverview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.contactsList = new System.Windows.Forms.ListBox();
            this.contactDetailBox = new System.Windows.Forms.GroupBox();
            this.calenderButton = new System.Windows.Forms.Button();
            this.surnameText = new System.Windows.Forms.TextBox();
            this.surnameLabel = new System.Windows.Forms.Label();
            this.phoneNumbersBox = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.deletePhoneNumberButton = new System.Windows.Forms.Button();
            this.phoneNumbersList = new System.Windows.Forms.ListBox();
            this.adresBox = new System.Windows.Forms.GroupBox();
            this.cityTextBox = new System.Windows.Forms.TextBox();
            this.cityLabel = new System.Windows.Forms.Label();
            this.postalcodeTextBox = new System.Windows.Forms.TextBox();
            this.postalcodeLabel = new System.Windows.Forms.Label();
            this.houseNumberTextBox = new System.Windows.Forms.TextBox();
            this.houseNumberLabel = new System.Windows.Forms.Label();
            this.streetTextBox = new System.Windows.Forms.TextBox();
            this.streetLabel = new System.Windows.Forms.Label();
            this.lastnameTextBox = new System.Windows.Forms.TextBox();
            this.lastnameLabel = new System.Windows.Forms.Label();
            this.firstnameTextBox = new System.Windows.Forms.TextBox();
            this.firstnameLabel = new System.Windows.Forms.Label();
            this.deleteContactButton = new System.Windows.Forms.Button();
            this.contactDetailBox.SuspendLayout();
            this.phoneNumbersBox.SuspendLayout();
            this.adresBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // contactsList
            // 
            this.contactsList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.contactsList.FormattingEnabled = true;
            this.contactsList.ItemHeight = 20;
            this.contactsList.Location = new System.Drawing.Point(14, 14);
            this.contactsList.Name = "contactsList";
            this.contactsList.Size = new System.Drawing.Size(476, 424);
            this.contactsList.TabIndex = 0;
            this.contactsList.SelectedIndexChanged += new System.EventHandler(this.ContactsListSelectedIndexChanged);
            // 
            // contactDetailBox
            // 
            this.contactDetailBox.Controls.Add(this.calenderButton);
            this.contactDetailBox.Controls.Add(this.surnameText);
            this.contactDetailBox.Controls.Add(this.surnameLabel);
            this.contactDetailBox.Controls.Add(this.phoneNumbersBox);
            this.contactDetailBox.Controls.Add(this.adresBox);
            this.contactDetailBox.Controls.Add(this.lastnameTextBox);
            this.contactDetailBox.Controls.Add(this.lastnameLabel);
            this.contactDetailBox.Controls.Add(this.firstnameTextBox);
            this.contactDetailBox.Controls.Add(this.firstnameLabel);
            this.contactDetailBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.contactDetailBox.Location = new System.Drawing.Point(499, 15);
            this.contactDetailBox.Name = "contactDetailBox";
            this.contactDetailBox.Size = new System.Drawing.Size(470, 507);
            this.contactDetailBox.TabIndex = 1;
            this.contactDetailBox.TabStop = false;
            this.contactDetailBox.Text = "Gegevens";
            // 
            // calenderButton
            // 
            this.calenderButton.Location = new System.Drawing.Point(13, 465);
            this.calenderButton.Name = "calenderButton";
            this.calenderButton.Size = new System.Drawing.Size(450, 31);
            this.calenderButton.TabIndex = 10;
            this.calenderButton.Text = "Kalender gegevens";
            this.calenderButton.UseVisualStyleBackColor = true;
            this.calenderButton.Click += new System.EventHandler(this.CalenderButton_Click);
            // 
            // surnameText
            // 
            this.surnameText.Location = new System.Drawing.Point(157, 57);
            this.surnameText.Name = "surnameText";
            this.surnameText.Size = new System.Drawing.Size(121, 26);
            this.surnameText.TabIndex = 2;
            this.surnameText.TextChanged += new System.EventHandler(this.SurnameTextTextChanged);
            // 
            // surnameLabel
            // 
            this.surnameLabel.AutoSize = true;
            this.surnameLabel.Location = new System.Drawing.Point(153, 30);
            this.surnameLabel.Name = "surnameLabel";
            this.surnameLabel.Size = new System.Drawing.Size(115, 20);
            this.surnameLabel.TabIndex = 8;
            this.surnameLabel.Text = "Tussenvoegsel";
            // 
            // phoneNumbersBox
            // 
            this.phoneNumbersBox.Controls.Add(this.button2);
            this.phoneNumbersBox.Controls.Add(this.deletePhoneNumberButton);
            this.phoneNumbersBox.Controls.Add(this.phoneNumbersList);
            this.phoneNumbersBox.Location = new System.Drawing.Point(13, 263);
            this.phoneNumbersBox.Name = "phoneNumbersBox";
            this.phoneNumbersBox.Size = new System.Drawing.Size(450, 195);
            this.phoneNumbersBox.TabIndex = 7;
            this.phoneNumbersBox.TabStop = false;
            this.phoneNumbersBox.Text = "Telefoon nummers";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(230, 157);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(213, 31);
            this.button2.TabIndex = 8;
            this.button2.Text = "Nieuw telefoonnummer";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.AddPhoneNumberButtonClick);
            // 
            // deletePhoneNumberButton
            // 
            this.deletePhoneNumberButton.Location = new System.Drawing.Point(7, 157);
            this.deletePhoneNumberButton.Name = "deletePhoneNumberButton";
            this.deletePhoneNumberButton.Size = new System.Drawing.Size(215, 31);
            this.deletePhoneNumberButton.TabIndex = 9;
            this.deletePhoneNumberButton.Text = "Verwijder";
            this.deletePhoneNumberButton.UseVisualStyleBackColor = true;
            this.deletePhoneNumberButton.Click += new System.EventHandler(this.DeletePhoneNumberButtonClick);
            // 
            // phoneNumbersList
            // 
            this.phoneNumbersList.FormattingEnabled = true;
            this.phoneNumbersList.ItemHeight = 20;
            this.phoneNumbersList.Location = new System.Drawing.Point(8, 30);
            this.phoneNumbersList.Name = "phoneNumbersList";
            this.phoneNumbersList.Size = new System.Drawing.Size(434, 104);
            this.phoneNumbersList.TabIndex = 0;
            // 
            // adresBox
            // 
            this.adresBox.Controls.Add(this.cityTextBox);
            this.adresBox.Controls.Add(this.cityLabel);
            this.adresBox.Controls.Add(this.postalcodeTextBox);
            this.adresBox.Controls.Add(this.postalcodeLabel);
            this.adresBox.Controls.Add(this.houseNumberTextBox);
            this.adresBox.Controls.Add(this.houseNumberLabel);
            this.adresBox.Controls.Add(this.streetTextBox);
            this.adresBox.Controls.Add(this.streetLabel);
            this.adresBox.Location = new System.Drawing.Point(13, 93);
            this.adresBox.Name = "adresBox";
            this.adresBox.Size = new System.Drawing.Size(450, 162);
            this.adresBox.TabIndex = 6;
            this.adresBox.TabStop = false;
            this.adresBox.Text = "Adres gegevens";
            // 
            // cityTextBox
            // 
            this.cityTextBox.Location = new System.Drawing.Point(145, 122);
            this.cityTextBox.Name = "cityTextBox";
            this.cityTextBox.Size = new System.Drawing.Size(298, 26);
            this.cityTextBox.TabIndex = 7;
            this.cityTextBox.TextChanged += new System.EventHandler(this.CityTextBoxTextChanged);
            // 
            // cityLabel
            // 
            this.cityLabel.AutoSize = true;
            this.cityLabel.Location = new System.Drawing.Point(140, 96);
            this.cityLabel.Name = "cityLabel";
            this.cityLabel.Size = new System.Drawing.Size(53, 20);
            this.cityLabel.TabIndex = 13;
            this.cityLabel.Text = "Plaats";
            // 
            // postalcodeTextBox
            // 
            this.postalcodeTextBox.Location = new System.Drawing.Point(7, 122);
            this.postalcodeTextBox.Name = "postalcodeTextBox";
            this.postalcodeTextBox.Size = new System.Drawing.Size(130, 26);
            this.postalcodeTextBox.TabIndex = 6;
            this.postalcodeTextBox.TextChanged += new System.EventHandler(this.PostalcodeTextBoxTextChanged);
            // 
            // postalcodeLabel
            // 
            this.postalcodeLabel.AutoSize = true;
            this.postalcodeLabel.Location = new System.Drawing.Point(2, 96);
            this.postalcodeLabel.Name = "postalcodeLabel";
            this.postalcodeLabel.Size = new System.Drawing.Size(76, 20);
            this.postalcodeLabel.TabIndex = 11;
            this.postalcodeLabel.Text = "Postcode";
            // 
            // houseNumberTextBox
            // 
            this.houseNumberTextBox.Location = new System.Drawing.Point(331, 53);
            this.houseNumberTextBox.Name = "houseNumberTextBox";
            this.houseNumberTextBox.Size = new System.Drawing.Size(111, 26);
            this.houseNumberTextBox.TabIndex = 5;
            this.houseNumberTextBox.TextChanged += new System.EventHandler(this.HouseNumberTextBoxTextChanged);
            // 
            // houseNumberLabel
            // 
            this.houseNumberLabel.AutoSize = true;
            this.houseNumberLabel.Location = new System.Drawing.Point(327, 27);
            this.houseNumberLabel.Name = "houseNumberLabel";
            this.houseNumberLabel.Size = new System.Drawing.Size(99, 20);
            this.houseNumberLabel.TabIndex = 9;
            this.houseNumberLabel.Text = "Huisnummer";
            // 
            // streetTextBox
            // 
            this.streetTextBox.Location = new System.Drawing.Point(7, 53);
            this.streetTextBox.Name = "streetTextBox";
            this.streetTextBox.Size = new System.Drawing.Size(317, 26);
            this.streetTextBox.TabIndex = 4;
            this.streetTextBox.TextChanged += new System.EventHandler(this.StreetTextBoxTextChanged);
            // 
            // streetLabel
            // 
            this.streetLabel.AutoSize = true;
            this.streetLabel.Location = new System.Drawing.Point(2, 27);
            this.streetLabel.Name = "streetLabel";
            this.streetLabel.Size = new System.Drawing.Size(53, 20);
            this.streetLabel.TabIndex = 7;
            this.streetLabel.Text = "Straat";
            // 
            // lastnameTextBox
            // 
            this.lastnameTextBox.Location = new System.Drawing.Point(286, 57);
            this.lastnameTextBox.Name = "lastnameTextBox";
            this.lastnameTextBox.Size = new System.Drawing.Size(177, 26);
            this.lastnameTextBox.TabIndex = 3;
            this.lastnameTextBox.TextChanged += new System.EventHandler(this.LastnameTextBoxTextChanged);
            // 
            // lastnameLabel
            // 
            this.lastnameLabel.AutoSize = true;
            this.lastnameLabel.Location = new System.Drawing.Point(281, 30);
            this.lastnameLabel.Name = "lastnameLabel";
            this.lastnameLabel.Size = new System.Drawing.Size(96, 20);
            this.lastnameLabel.TabIndex = 2;
            this.lastnameLabel.Text = "Achternaam";
            // 
            // firstnameTextBox
            // 
            this.firstnameTextBox.Location = new System.Drawing.Point(13, 57);
            this.firstnameTextBox.Name = "firstnameTextBox";
            this.firstnameTextBox.Size = new System.Drawing.Size(137, 26);
            this.firstnameTextBox.TabIndex = 1;
            this.firstnameTextBox.TextChanged += new System.EventHandler(this.FirstnameTextBoxTextChanged);
            // 
            // firstnameLabel
            // 
            this.firstnameLabel.AutoSize = true;
            this.firstnameLabel.Location = new System.Drawing.Point(8, 30);
            this.firstnameLabel.Name = "firstnameLabel";
            this.firstnameLabel.Size = new System.Drawing.Size(83, 20);
            this.firstnameLabel.TabIndex = 0;
            this.firstnameLabel.Text = "Voornaam";
            // 
            // deleteContactButton
            // 
            this.deleteContactButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.deleteContactButton.Location = new System.Drawing.Point(15, 480);
            this.deleteContactButton.Name = "deleteContactButton";
            this.deleteContactButton.Size = new System.Drawing.Size(477, 31);
            this.deleteContactButton.TabIndex = 10;
            this.deleteContactButton.Text = "Verwijder contact";
            this.deleteContactButton.UseVisualStyleBackColor = true;
            this.deleteContactButton.Click += new System.EventHandler(this.DeleteContactButtonClick);
            // 
            // ContactsOverview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(983, 530);
            this.Controls.Add(this.deleteContactButton);
            this.Controls.Add(this.contactDetailBox);
            this.Controls.Add(this.contactsList);
            this.Name = "ContactsOverview";
            this.Text = "Adressen boek";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.ContactsOverview_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ContactsOverview_FormClosing);
            this.contactDetailBox.ResumeLayout(false);
            this.contactDetailBox.PerformLayout();
            this.phoneNumbersBox.ResumeLayout(false);
            this.adresBox.ResumeLayout(false);
            this.adresBox.PerformLayout();
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.ListBox contactsList;
        private System.Windows.Forms.GroupBox contactDetailBox;
        private System.Windows.Forms.TextBox lastnameTextBox;
        private System.Windows.Forms.Label lastnameLabel;
        private System.Windows.Forms.TextBox firstnameTextBox;
        private System.Windows.Forms.Label firstnameLabel;
        private System.Windows.Forms.GroupBox adresBox;
        private System.Windows.Forms.TextBox streetTextBox;
        private System.Windows.Forms.Label streetLabel;
        private System.Windows.Forms.TextBox postalcodeTextBox;
        private System.Windows.Forms.Label postalcodeLabel;
        private System.Windows.Forms.TextBox houseNumberTextBox;
        private System.Windows.Forms.Label houseNumberLabel;
        private System.Windows.Forms.TextBox cityTextBox;
        private System.Windows.Forms.Label cityLabel;
        private System.Windows.Forms.GroupBox phoneNumbersBox;
        private System.Windows.Forms.Button deletePhoneNumberButton;
        private System.Windows.Forms.ListBox phoneNumbersList;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox surnameText;
        private System.Windows.Forms.Label surnameLabel;
        private System.Windows.Forms.Button deleteContactButton;
        private System.Windows.Forms.Button calenderButton;
    }
}

