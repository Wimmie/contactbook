﻿using System.Linq;
using System.Windows.Forms;
using ContactBook.Classes;

namespace ContactBook.Screens
{
    public partial class Calendar : Form
    {
        private readonly Person _person;

        public Calendar(Person person)
        {
            _person = person;
            InitializeComponent();
        }

        private void AddButton_Click(object sender, System.EventArgs e)
        {
            new AddCalendar(_person).ShowDialog();
        }

        private void Calendar_Activated(object sender, System.EventArgs e)
        {
            calendarListBox.DataSource = _person.CalendarEntries.OrderBy(x => x.Date).ToList();
            calendarListBox.DisplayMember = "";
            calendarListBox.DisplayMember = "EntryAsString";
        }

        private void DeleteButton_Click(object sender, System.EventArgs e)
        {
            var calendarEntry = (CalendarEntry) calendarListBox.SelectedItem;
            _person.CalendarEntries.Remove(calendarEntry);
            calendarListBox.DataSource = _person.CalendarEntries.OrderBy(x => x.Date).ToList();
        }
    }
}
