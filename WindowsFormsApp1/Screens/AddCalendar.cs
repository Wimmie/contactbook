﻿using System;
using System.Windows.Forms;
using ContactBook.Classes;

namespace ContactBook.Screens
{
    public partial class AddCalendar : Form
    {
        private readonly Person _person;

        public AddCalendar(Person person)
        {
            InitializeComponent();
            _person = person;
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            _person.AddCalendarEntry(dateTimePicker.Value, descriptionTextBox.Text);
            Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}